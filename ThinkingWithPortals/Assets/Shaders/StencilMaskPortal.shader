﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Stencil/StencilMaskPortal"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_StencilMaskMask("Mask Mask",int) = 0
		_StencilMask("Stencil Mask",int) = 0
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque" "Queue" = "Geometry-100" }
		ColorMask 0
		ZWrite off

		Stencil{
		Ref[_StencilMask]
		Comp always
		Pass replace
	}
			Stencil{
			Ref[_StencilMaskMask]
			Comp always
		}

	Pass
	{
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
				// make fog work
		#pragma multi_compile_fog

		#include "UnityCG.cginc"

			struct appdata
		{
			float4 vertex : POSITION;
			float2 uv : TEXCOORD0;
			float3 norm : NORMAL;
		};

		struct v2f
		{
			float2 uv : TEXCOORD0;
			UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			float3 norm : NORMAL;
		};

		sampler2D _MainTex;
		float4 _MainTex_ST;

		v2f vert(appdata v)
		{
			v2f o;
			//float3 v0 = mul(unity_ObjectToWorld, v.vertex).xyz;
			//v.vertex.y += clamp(cos(v.vertex.x * 4),0.5,0.5);
			//v.vertex.xyz += v.norm.xyz *_CosTime.w;
			//v.vertex.xyz = mul((float3x3)unity_WorldToObject, v0);

			o.uv = TRANSFORM_TEX(v.uv, _MainTex);
			o.vertex = UnityObjectToClipPos(v.vertex);
			o.norm = v.norm;
			UNITY_TRANSFER_FOG(o,o.vertex);
			return o;
		}

		fixed4 frag(v2f i) : SV_Target
		{
			// sample the texture
			fixed4 col = tex2D(_MainTex, i.uv);
			// apply fog
			UNITY_APPLY_FOG(i.fogCoord, col);
			return col;
		}
			ENDCG
		}
	}
}