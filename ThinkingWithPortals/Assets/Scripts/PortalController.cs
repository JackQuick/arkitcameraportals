﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalController : MonoBehaviour {

    public Material CameraMask;


    void Start(){
        CameraMask.SetInt("_StencilMaskMask", 1);
    }


    public void OnTriggerExit(Collider other) {
        Debug.Log("!!!!!!!!!!!!!!!Triggerd Exit");
        if (other.transform.position.z > transform.position.z) {
            //set mask to 1
            Debug.Log("!!!!!!!!!!!!!!!!!!Set to 1");
            CameraMask.SetInt("_StencilMaskMask", 1);
        } else {
            //set mask to 2
            Debug.Log("!!!!!!!!!!!!!!!!!!Set to 2");
            CameraMask.SetInt("_StencilMaskMask", 2);
        }
    }
    public void OnTriggerEnter(Collider other) {
        Debug.Log("!!!!!!!!!!!!!!!Triggerd Enter");
        if (other.transform.position.z < transform.position.z) {
            //set mask to 1
            Debug.Log("!!!!!!!!!!!!!!!!!!Set to 1");
            CameraMask.SetInt("_StencilMaskMask", 1);
        } else {
            //set mask to 2
            Debug.Log("!!!!!!!!!!!!!!!!!!Set to 2");
            CameraMask.SetInt("_StencilMaskMask", 2);
        }
    }

    private void OnApplicationQuit() {
        //set mask to 3
        CameraMask.SetInt("_StencilMaskMask", 3);
    }
}
