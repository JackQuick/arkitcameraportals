﻿using UnityEngine;
using System.Collections;
#if UNITY_IOS
using UnityEngine.iOS;
#endif

public class Portal : MonoBehaviour {

    [Header("Cameras")]
    public Camera mainCam;
    public Camera portalCam;




    void Update() {



        // Move portal camera position based on main camera distance from the portal 

        portalCam.transform.position = mainCam.transform.position;
        portalCam.transform.rotation = mainCam.transform.rotation;

        // Make portal cam face the same direction as the main camera - this is the original code 
        //portalCam.transform.rotation = Quaternion.LookRotation(mainCam.transform.forward, Vector3.up);
    }

    //-------------------------------------------------------------------------------------

    void OnTriggerEnter(Collider other) {
        if (other.CompareTag("MainCamera")) {
            // Use xor operator to toggle the ARWorld layer in the mainCam's culling mask.
            mainCam.cullingMask ^= 1 << LayerMask.NameToLayer("ARWorld");


        }
    }




}


