﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitcher : MonoBehaviour {


    private static string scene1 = "FramedPortal";
    private static string scene3 = "PGTableTop";
    private static string scene2 = "AtlasWonder";

    Scene currentScene;
    public void Start() {
        currentScene = SceneManager.GetActiveScene();
    }

    public void SwitchScene() {

        if (currentScene.name.Equals(scene1)) {
            SceneManager.LoadScene(scene2, LoadSceneMode.Single);
        } else if (currentScene.name.Equals(scene2)) {
            SceneManager.LoadScene(scene3, LoadSceneMode.Single);
        } else {
            SceneManager.LoadScene(scene1, LoadSceneMode.Single);
        }
    }


}
