# Thinking With Portals

<p align="center">
  <img src="readme_img/example-run.gif" width="266">
</p>

portals inside Unity using a combination of Render Textures, Depth Masks, and Layers.

unity2017.3.0p2


Portal shader from https://github.com/pr0g/unity-portal-rendering